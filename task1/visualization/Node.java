/*
 * Copyright (c) 2006 Intel Corporation
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached INTEL-LICENSE
 * file. If you do not find these files, copies can be found by writing to
 * Intel Research Berkeley, 2150 Shattuck Avenue, Suite 1300, Berkeley, CA,
 * 94704.  Attention:  Intel License Inquiry.
 */
import java.util.*;

/**
 * Class holding all data received from a mote.
 */
class Node {
    /* The mote's identifier */
    int id;

    /* Data received from the mote. data[0] is the dataStart'th sample
       Indexes 0 through dataEnd - dataStart - 1 hold data.
       Samples are 16-bit unsigned numbers, -1 indicates missing data. */
    ArrayList<RecordMsg> data = new ArrayList<RecordMsg>();
    int dataStart;

    Node(int _id) {
        id = _id;
    }

    /* Data received containing NREADINGS samples from messageId * NREADINGS
       onwards */
    void update(RecordMsg msg) {
        if (data.isEmpty())
            dataStart = msg.get_time();

        int index = msg.get_time()-dataStart;
        if (index<0)
            return;

        //Fill with empty data
        while (data.size()<=index)
            data.add(null);
        //Set data
        data.set(index, msg);
    }

    /* Return value of sample x, or -1 for missing data */
    int getTemperature(int x) {
        try {
            int arrayIndex = x/Constants.SAMPLES_PER_PACKET,
                msgIndex = x%Constants.SAMPLES_PER_PACKET;

            return data.get(arrayIndex).get_temperature()[msgIndex];
        } catch (Exception e) {
            return -1;
        }
    }

    int getHumidity(int x) {
        try {
            int arrayIndex = x/Constants.SAMPLES_PER_PACKET,
                msgIndex = x%Constants.SAMPLES_PER_PACKET;

            return data.get(arrayIndex).get_humidity()[msgIndex];
        } catch (Exception e) {
            return -1;
        }
    }

    int getLight(int x) {
        try {
            int arrayIndex = x/Constants.SAMPLES_PER_PACKET,
                msgIndex = x%Constants.SAMPLES_PER_PACKET;

            return data.get(arrayIndex).get_light()[msgIndex];
        } catch (Exception e) {
            return -1;
        }
    }

    /* Return number of last known sample */
    int maxTime() {
        return data.size()*Constants.SAMPLES_PER_PACKET;
    }
}
