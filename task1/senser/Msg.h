#ifndef MSG_H
#define MSG_H

enum {
    AM_RECORDMSG = 6,
    AM_CONTROLMSG = 7,
    INIT_SAMPLING_FREQUENCY = 100,
    BASE_STATION_ID = 1,
    FORWARDER_ID = 2,
    SENSER_ID = 3,
    SAMPLES_PER_PACKET = 5
};

typedef nx_struct RecordMsg {
    nx_uint8_t nodeId;
    nx_uint16_t time;
    nx_uint16_t temperature[SAMPLES_PER_PACKET];
    nx_uint16_t humidity[SAMPLES_PER_PACKET];
    nx_uint16_t light[SAMPLES_PER_PACKET];
    nx_uint16_t version;
    nx_uint16_t frequency;
} RecordMsg;

typedef nx_struct Sample {
    nx_uint16_t temperature;
    nx_uint16_t humidity;
    nx_uint16_t light;
} Sample;

typedef nx_struct ControlMsg {
    nx_uint8_t rootId;
    nx_uint16_t confirmTime;
    nx_uint16_t samplingFrequency;
    nx_uint16_t frequencyVersion;
} ControlMsg;

#endif
