//SenseC.nc: Sense node component
#include "Timer.h"
#include "printf.h"
#include "SensirionSht11.h"
#include "Msg.h"

module SenseC
{
    uses {
        interface Boot;
        interface Leds;
        interface Timer<TMilli> as TimerSense;
        //interface Timer<TMilli> as TimerSend;
        interface Read<uint16_t> as getTemperature;
        interface Read<uint16_t> as getHumidity;
        interface Read<uint16_t> as getLight;
        interface Packet;
        interface AMPacket;
        interface AMSend;
        interface Receive;
        interface SplitControl as RadioControl;
    }
}
implementation
{
    enum {
            SEND_QUEUE_LEN = 12
    };
    /*global state*/
    #define SEND_INTERVAL 100
    //#define RECEIVE_INTERVAL 100
    // sampling frequency in binary milliseconds
    Sample samples[SAMPLES_PER_PACKET];
    uint8_t n_samples = 0;
    uint8_t collect_state = 0; //once reached 3, collect sample

    RecordMsg content;
    message_t sendQueue[SEND_QUEUE_LEN];
    uint32_t queueBegin = 0;
    uint32_t queueEnd = 0;

    bool sendBusy = FALSE;
    bool isAck = TRUE;
    bool suppressCountChange = FALSE;

    void sendData(RecordMsg* msg);

    /*--------------------*/

    event void Boot.booted() {
        //init the packet properties
        content.nodeId = TOS_NODE_ID;
        content.version = 0;
        content.time = 0;
        content.frequency = INIT_SAMPLING_FREQUENCY;

        call RadioControl.start();
    }

    void startTimer() {
        call TimerSense.startPeriodic(content.frequency);
        //call TimerSend.startPeriodic(content.frequency/2);
    }

    void stopTimer() {
        call TimerSense.stop();
        //call TimerSend.stop();
    }

    event void RadioControl.startDone(error_t err){
        if(err == SUCCESS){
            startTimer();
        } else {
            call RadioControl.start();
        }
    }

    event void RadioControl.stopDone(error_t error){
        printf("Radio control stop.\n");
    }

    /*----------sense part------------------------------*/
    event void TimerSense.fired() {
        if (!sendBusy && isAck && collect_state == 0) {
            call getTemperature.read();
            call getHumidity.read();
            call getLight.read();
        }
    }

    //Check sense data collection status and send data
    void checkSendData() {
        //Round pending
        if (collect_state<3)
            return;
        //Round finished
        else
            collect_state = 0;

        //Update samples amount
        n_samples++;
        if (n_samples>=SAMPLES_PER_PACKET) {
            uint8_t i;

            printf("Ready to send data.\n");

            //Update time
            content.time += 1;
            //Copy samples data to content
            for (i=0;i<SAMPLES_PER_PACKET;i++) {
                content.temperature[i] = samples[i].temperature;
                content.humidity[i] = samples[i].humidity;
                content.light[i] = samples[i].light;
            }
            //Send data
            sendData(&content);
            //Reset samples amount
            n_samples = 0;
        }
    }

    event void getTemperature.readDone(error_t result, uint16_t data) {
        if (result == SUCCESS) {
            uint16_t temperature = (uint16_t)((data * 0.01 - 40.1) - 32) / 1.8;
            //uint16_t temperature = (uint16_t)(data * 0.01 - 39.60);

            samples[n_samples].temperature = temperature;
            collect_state++;
            checkSendData();

            printf("Temperature: %d\n", temperature);
        }
        //failed
        else {
            printf("Temperature sensor failed. Trying again...\n");
            call getTemperature.read();
        }
    }

    event void getHumidity.readDone(error_t result, uint16_t data) {
        if(result == SUCCESS) {
            //uint16_t humidity = data;
            uint16_t humidity = (uint16_t)(-4 + 0.0405 * data + (-2.8 * 0.00001) * (data * data));

            samples[n_samples].humidity = humidity;
            collect_state++;
            checkSendData();

            printf("Humidity: %d\n", humidity);
        }
        //failed
        else {
            printf("Humidity sensor failed. Trying again...\n");
            call getHumidity.read();
        }
    }

    event void getLight.readDone(error_t result, uint16_t data) {
        if(result == SUCCESS) {
            uint16_t vol = data * 1.5 / 4.096;
            uint16_t light = 0.625 * 10 * vol;

            printf("Light data: %u\n", data);

            samples[n_samples].light = light;
            collect_state++;
            checkSendData();

            printf("Light: %d\n", light);
        }
        //failed
        else {
            printf("Light sensor failed. Trying again...\n");
            call getLight.read();
        }
    }

    /*--------------------------------------------*/
    /*------------------send part-----------------------*/
    //Send data task
    task void sendTask() {
        message_t* recordPacket;

        recordPacket = sendQueue+queueEnd%SEND_QUEUE_LEN;
        //Try to send packet
        if (call AMSend.send(AM_BROADCAST_ADDR, recordPacket, sizeof(RecordMsg))==SUCCESS) {
            //Busy state
            sendBusy = TRUE;

            call Leds.led1Toggle();
        } else {
            //Update queue end
            queueEnd++;
            //Next send task
            if (queueBegin>queueEnd) {
                post sendTask();
            }

            call Leds.led0Toggle();
        }
    }

    //Send sense data
    void sendData(RecordMsg* msg) {
        message_t* packet;
        void* payload;

        //Queue full
        if (queueEnd+SEND_QUEUE_LEN<=queueBegin)
            return;

        packet = sendQueue+queueBegin%SEND_QUEUE_LEN;
        payload = call AMSend.getPayload(packet, sizeof(RecordMsg));
        //Copy data
        memcpy(payload, msg, sizeof(RecordMsg));

        //Start send task
        if (queueBegin==queueEnd) {
            post sendTask();
        }
        //Update queue begin
        queueBegin++;
    }

    event void AMSend.sendDone(message_t* msg, error_t err) {
        sendBusy = FALSE;
        queueEnd++;

        //Next send task
        if (queueBegin>queueEnd) {
            post sendTask();
        }

        if (err==SUCCESS) {
            call Leds.led1Toggle();
        } else {
            call Leds.led0Toggle();
        }
    }
    /*---------------------------------------------------*/
    /*-----------------------receive part--------------------*/
    event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len) {
        //receive control part
        if (len == sizeof(ControlMsg)){
            ControlMsg* receiveControl = (ControlMsg*)payload;

            call Leds.led2Toggle();

            // if(content.time == receiveControl->confirmTime){
            //   isAck = TRUE;
            // }
            //base station control intervel
            if(receiveControl->frequencyVersion > content.version){
                content.version = receiveControl->frequencyVersion;
                content.frequency = receiveControl->samplingFrequency;

                stopTimer();
                startTimer();
            }
        }
        return msg;
    }
    /*---------------------------------------------------*/
}
