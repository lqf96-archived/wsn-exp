//Constants
enum {
    N_INTEGERS_PER_TASK = 10
};

//Result packet
message_t resultPacket;
//Result data
ResultMsg resultMsg;
//Result state
uint8_t resultState;
//Result send busy
bool resultSendBusy = FALSE;

//Sum of numbers
uint32_t sum = 0;
//Current index
uint16_t index = 0;

//Task completion status
bool taskACompleted = FALSE;
bool taskBCompleted = FALSE;
bool taskCCompleted = FALSE;

//Check and send result
void checkSendResult() {
    void* payload;

    //Check status
    if (!(taskACompleted&&taskBCompleted&&taskCCompleted)) {
        return;
    }

    //Group ID
    resultMsg.groupId = GROUP;
    //Copy payload
    payload = call ResultMsgSender.getPayload(&resultPacket, sizeof(ResultMsg));
    memcpy(payload, &resultMsg, sizeof(ResultMsg));

    //Reset state
    resultState = 0;
    //Start timeout timer
    call ResultTimeoutTimer.startOneShot(TIMEOUT);

    //Send packet
    if (call ResultMsgSender.send(RESULT_NODE, &resultPacket, sizeof(ResultMsg))==SUCCESS) {
        resultSendBusy = TRUE;
        call Leds.led2Toggle();
    } else {
        call Leds.led0Toggle();
    }
}

//Calculation task
task void calcTask() {
    uint16_t maxIndex = MIN(index+N_INTEGERS_PER_TASK, N_INTEGERS);

    while (index<maxIndex) {
        sum += integers[index];
        index++;
    }

    if (index>=N_INTEGERS) {
        resultMsg.sum = sum;
        resultMsg.average = sum/N_INTEGERS;

        DEBUG_PRINT("[NodeB calcTask] sum: %lu, average: %lu\n", sum, sum/N_INTEGERS);

        taskBCompleted = TRUE;
        checkSendResult();
    } else {
        post calcTask();
    }
}

//Partial result handler
void partialResultHandler(DataMsg* msg) {
    PartialResultMsg* partResult1 = (PartialResultMsg*)msg;
    uint8_t nodeId = partResult1->nodeId;
    PartialResultMsg partResultAck;

    if (nodeId==NODE_ID(3)) {
        resultMsg.max = partResult1->results[0];
        resultMsg.min = partResult1->results[1];

        DEBUG_PRINT("[NodeB partialResultHandler] Result from node 3 (A): max: %lu, min: %lu \n", resultMsg.max, resultMsg.min);

        taskACompleted = TRUE;
    } else if (nodeId==NODE_ID(2)) {
        resultMsg.median = partResult1->results[0];

        DEBUG_PRINT("[NodeB partialResultHandler] Result from node 2 (C): median: %lu\n", resultMsg.median);

        taskCCompleted = TRUE;
    }
    checkSendResult();

    //Send ACK
    partResultAck.type = PARTIAL_RESULT_ACK_MSG;
    partResultAck.nodeId = nodeId;
    sendData((DataMsg*)(&partResultAck));
}

//Result send done
event void ResultMsgSender.sendDone(message_t* packet, error_t err) {
    resultSendBusy = FALSE;

    if (err==SUCCESS) {
        call Leds.led2Toggle();
    } else {
        call Leds.led0Toggle();
    }
}

//Result ACK received
event message_t* ResultAckMsgReceiver.receive(message_t* packet, void* payload, uint8_t len) {
    ResultAckMsg* resultAckMsg;

    if (len!=sizeof(ResultAckMsg)) {
        return packet;
    }

    //Update result state
    if (resultAckMsg->groupId==GROUP) {
        resultState++;

        DEBUG_PRINT("[NodeB ResultAckMsgReceiver.receive] Received result ACK.\n");
    }

    return packet;
    //End of node B logic
    //The node should do nothing after execution of this function
}

//Result send timeout
event void ResultTimeoutTimer.fired() {
    //No timeout
    if (resultState>=1) {
        return;
    }

    DEBUG_PRINT("[NodeB ResultTimeoutTimer.fired] Result timeout. Retransmitting...\n");

    //Reset state
    resultState = 0;
    //Start timeout timer
    call ResultTimeoutTimer.startOneShot(TIMEOUT);

    //Send packet
    if (call ResultMsgSender.send(AM_BROADCAST_ADDR, &resultPacket, sizeof(ResultMsg))==SUCCESS) {
        resultSendBusy = TRUE;
        call Leds.led2Toggle();
    } else {
        call Leds.led0Toggle();
    }
}

//Self query completed hook
void selfQueryCompleted() {
    post calcTask();
}

//All queries completed hook (Nothing)
void allQueriesCompleted() {}

//Data message dispatch table
BEGIN_DATA_MSG_DISPATCH
    DATA_MSG_MAP(QUERY_MSG, queryHandler)
    DATA_MSG_MAP(QUERY_RESP_MSG, queryRespHandler)
    DATA_MSG_MAP(PARTIAL_RESULT_MSG, partialResultHandler)
END_DATA_MSG_DISPATCH
