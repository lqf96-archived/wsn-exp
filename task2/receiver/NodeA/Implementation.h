enum {
    N_INTEGERS_PER_TASK = 10
};

//Minimum and maximum number
uint32_t minNum = 0xffff;
uint32_t maxNum = 0;
//Current index
uint16_t index = 0;

//Calculation task
task void calcTask() {
    uint16_t maxIndex = MIN(index+N_INTEGERS_PER_TASK, N_INTEGERS);

    while (index<maxIndex) {
        minNum = MIN(integers[index], minNum);
        maxNum = MAX(integers[index], maxNum);

        index++;
    }

    if (maxIndex>=N_INTEGERS) {
        partResult.type = PARTIAL_RESULT_MSG;
        partResult.nodeId = TOS_NODE_ID;
        partResult.results[0] = maxNum;
        partResult.results[1] = minNum;

        DEBUG_PRINT("[NodeA calcTask] min: %lu, max: %lu\n", minNum, maxNum);

        //Reset state
        partResultState = 0;
        //Start timeout timer
        call PartialResultTimeoutTimer.startOneShot(TIMEOUT);
        //Send data
        sendData((DataMsg*)(&partResult));
    } else {
        post calcTask();
    }
}

//Self query completed hook
void selfQueryCompleted() {
    post calcTask();
}

//All queries completed hook (Nothing)
void allQueriesCompleted() {}

//Data message dispatch table
BEGIN_DATA_MSG_DISPATCH
    DATA_MSG_MAP(QUERY_MSG, queryHandler)
    DATA_MSG_MAP(QUERY_RESP_MSG, queryRespHandler)
    DATA_MSG_MAP(PARTIAL_RESULT_ACK_MSG, partialResultAckHandler)
END_DATA_MSG_DISPATCH
