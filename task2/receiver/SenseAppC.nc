configuration SenseAppC {
}

implementation {
    components SenseC, MainC, LedsC;
    //Radio control
    components ActiveMessageC;
    //Senders
    components new AMSenderC(AM_DATAMSG) as DataMsgSender;
    //Receivers
    components new AMReceiverC(AM_NUMBERMSG) as NumberMsgReceiver;
    components new AMReceiverC(AM_DATAMSG) as DataMsgReceiver;
    //Timers
    components new TimerMilliC() as QueryTimeoutTimer;
    components new TimerMilliC() as PartialResultTimeoutTimer;

    //Node-specific components
    #if INTERNAL_ID == 3
    #include "NodeA/Components.h"
    #elif INTERNAL_ID == 1
    #include "NodeB/Components.h"
    #elif INTERNAL_ID == 2
    #include "NodeC/Components.h"
    #endif

    SenseC -> MainC.Boot;
    SenseC.Leds -> LedsC;
    //Radio control
    SenseC.RadioControl -> ActiveMessageC;
    //Packet
    SenseC.NumberMsgPacket -> NumberMsgReceiver;
    //Senders
    SenseC.DataMsgSender -> DataMsgSender;
    //Receivers
    SenseC.NumberMsgReceiver -> NumberMsgReceiver;
    SenseC.DataMsgReceiver -> DataMsgReceiver;
    //Timers
    SenseC.QueryTimeoutTimer -> QueryTimeoutTimer;
    SenseC.PartialResultTimeoutTimer -> PartialResultTimeoutTimer;
}
