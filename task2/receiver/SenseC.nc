//SenseC.nc: Sense node component
#include "Timer.h"
#include "printf.h"
#include "Msg.h"

module SenseC {
    uses {
        interface Boot;
        interface Leds;
        //Radio control
        interface SplitControl as RadioControl;
        //Packets
        interface AMPacket as NumberMsgPacket;
        //Senders
        interface AMSend as DataMsgSender;
        //Receivers
        interface Receive as NumberMsgReceiver;
        interface Receive as DataMsgReceiver;
        //Timers
        interface Timer<TMilli> as QueryTimeoutTimer;
        interface Timer<TMilli> as PartialResultTimeoutTimer;

        //Node-specific uses
        #if INTERNAL_ID == 3
        #include "NodeA/Uses.h"
        #elif INTERNAL_ID == 1
        #include "NodeB/Uses.h"
        #elif INTERNAL_ID == 2
        #include "NodeC/Uses.h"
        #endif
    }
}

//Min and max
#define MAX(x, y) ((x>y)?(x):(y))
#define MIN(x, y) ((x<y)?(x):(y))
//Node ID
#define NODE_ID(x) ((GROUP-1)*3+x)
//Data message dispatch table
#define BEGIN_DATA_MSG_DISPATCH \
    event message_t* DataMsgReceiver.receive(message_t* packet, void* payload, uint8_t len) { \
        DataMsg* msg; \
        if (len!=sizeof(DataMsg)) return packet; \
        msg = (DataMsg*)payload;
#define DATA_MSG_MAP(typeName, handler) if (msg->type==typeName) { handler(msg); }
#define END_DATA_MSG_DISPATCH return packet; }

#ifndef PROD_MODE
//Debug print
#define DEBUG_PRINT(...) { printf(__VA_ARGS__); printfflush(); }
#else
#define DEBUG_PRINT(...)
#endif

implementation {
    //Integers
    uint16_t integers[N_INTEGERS+1];
    //Last index
    uint16_t lastIndex = 0;

    //Data packet queue
    message_t dataQueue[DATA_QUEUE_LEN];
    //Queue begin and end
    uint16_t dataBegin = 0;
    uint16_t dataEnd = 0;
    //Data send busy
    bool dataSendBusy = FALSE;

    //Unknown numbers amount
    uint16_t unknownNumbers = N_INTEGERS;
    //Check & collect numbers index
    uint16_t checkCollectIndex = 0;
    //Query message
    QueryMsg queryMsg;
    //Query state (Number of response received)
    uint8_t queryState = 0;

    //Query completion status
    bool queryComplete[3];
    //Handle number message flag
    bool handleNumberMsgFlag = TRUE;
    //All queries completed hook called
    bool allQueriesCompletedCalled = FALSE;

    //Partial result
    PartialResultMsg partResult;
    //Partial result state
    uint8_t partResultState = 0;

    //Data message handler
    void queryHandler(DataMsg* msg);
    void queryRespHandler(DataMsg* msg);
    void partialResultAckHandler(DataMsg* msg);
    //Send data
    void sendData(DataMsg* msg);
    //Print array
    void printArray(const char* prefix, const uint16_t* array, uint16_t length);

    //Node-specific code
    #if INTERNAL_ID == 3
    #include "NodeA/Implementation.h"
    #elif INTERNAL_ID == 1
    #include "NodeB/Implementation.h"
    #elif INTERNAL_ID == 2
    #include "NodeC/Implementation.h"
    #endif

    //Print array (Debug only)
    void printArray(const char* prefix, const uint16_t* array, uint16_t length) {
        uint16_t i;
        char nullChar = '\0';

        if (!prefix) {
            prefix = &nullChar;
        }

        DEBUG_PRINT("%s %u [", prefix, length);
        if (length==0) {
            DEBUG_PRINT("]\n");
        } else {
            for (i=0;i<length;i++) {
                uint16_t data = array[i];

                if (i<length-1) {
                    DEBUG_PRINT("%u, ", data);
                } else {
                    DEBUG_PRINT("%u]\n", data);
                }
            }
        }
    }

    //Booted
    event void Boot.booted() {
        //Initialize integers
        memset(integers, 0xff, sizeof(uint16_t)*N_INTEGERS);
        //Initialize query completion status
        memset(queryComplete, 0, sizeof(bool)*3);

        //Query message object
        queryMsg.type = QUERY_MSG;
        queryMsg.nodeId = TOS_NODE_ID;

        DEBUG_PRINT("[Boot.booted] Boot.\n");

        //Start radio
        call RadioControl.start();
    }

    //Radio control started
    event void RadioControl.startDone(error_t err) {
        if (err != SUCCESS) {
            call RadioControl.start();
        }
    }

    //Radio control stopped (Nothing)
    event void RadioControl.stopDone(error_t error) {}

    //Check and collect numbers
    task void checkCollectNumbers() {
        uint16_t maxIndex = MIN(checkCollectIndex+N_INTEGERS_PER_TASK, N_INTEGERS);
        bool sendQueryFlag = FALSE;

        while ((checkCollectIndex<maxIndex)&&(queryMsg.length<MAX_QUERIES)) {
            //Number needs to be queried
            if (integers[checkCollectIndex]==0xffff) {
                //Add to query message object
                queryMsg.integers[queryMsg.length] = checkCollectIndex;
                queryMsg.length++;
            }

            if (checkCollectIndex%DEBUG_PRINT_INTERVAL==0) {
                DEBUG_PRINT("[checkCollectNumbers] index: %u, unknown: %u\n", checkCollectIndex, unknownNumbers);
            }
            checkCollectIndex++;
        }
        //checkCollectIndex += maxIndex;

        //* No unknown numbers (Send completion notification)
        //* Some queries to be sent
        if (unknownNumbers==0) {
            sendQueryFlag = TRUE;
        } else if (checkCollectIndex<N_INTEGERS) {
            //Query message full
            if (queryMsg.length>=MAX_QUERIES) {
                sendQueryFlag = TRUE;
            } else {
                post checkCollectNumbers();
            }
        } else if (queryMsg.length>0) {
            sendQueryFlag = TRUE;
        }

        //Send query
        if (sendQueryFlag) {
            //Print unknown numbers
            printArray("[checkCollectNumbers] indexes:", queryMsg.integers, queryMsg.length);

            //Reset query state
            queryState = 0;
            //Start timeout timer
            call QueryTimeoutTimer.startOneShot(TIMEOUT);
            //Send query message
            sendData((DataMsg*)(&queryMsg));
        }
    }

    //Number message received
    event message_t* NumberMsgReceiver.receive(message_t* packet, void* payload, uint8_t len) {
        NumberMsg* numberMsg;
        uint16_t index1;
        uint16_t integer;

        //From node 1000
        if (call NumberMsgPacket.source(packet)!=SOURCE_NODE)
            return packet;
        //Handle number message flag
        if (!handleNumberMsgFlag)
            return packet;
        //Not a number message
        if (len!=sizeof(NumberMsg))
            return packet;

        //Get sequence number and payload
        numberMsg = (NumberMsg*)payload;
        index1 = numberMsg->seqNumber-1;
        integer = (uint16_t)numberMsg->integer;

        if (index1%DEBUG_PRINT_INTERVAL==0) {
            DEBUG_PRINT("[NumberMsgReceiver.receive] index: %u, number: %u, unknown: %u\n", index1, integer, unknownNumbers);
        }

        //Update numbers store
        if (integers[index1]==0xffff) {
            integers[index1] = integer;
            unknownNumbers--;
        }

        //All numbers known
        if (unknownNumbers==0) {
            DEBUG_PRINT("[NumberMsgReceiver.receive] All numbers received. Broadcasting...\n");

            //Set query message object
            queryMsg.length = 0;

            //Do not handle number message any more
            handleNumberMsgFlag = FALSE;

            //Reset query state
            queryState = 0;
            //Start timeout timer
            call QueryTimeoutTimer.startOneShot(TIMEOUT);
            //Send query message
            sendData((DataMsg*)(&queryMsg));

            return packet;
        }

        //Check and collect numbers
        //When data fully collected, do not checkCollectNumbers() again
        if (lastIndex>index1) {
            //(Re)set query message object
            queryMsg.length = 0;
            //(Re)set counter
            checkCollectIndex = 0;

            DEBUG_PRINT("[NumberMsgReceiver.receive] Entering checkCollectNumbers.\n");

            post checkCollectNumbers();
        }
        //Update last integer
        lastIndex = index1;

        return packet;
    }

    //Check if all queries completed
    void checkAllQueriesCompleted() {
        uint8_t i;

        for (i=0;i<3;i++) {
            if (!queryComplete[i]) {
                return;
            }
        }

        //All queries completed hook
        //(Ensure it's only called once)
        if (!allQueriesCompletedCalled) {
            DEBUG_PRINT("[checkAllQueriesCompleted] All queries completed.\n");

            allQueriesCompletedCalled = TRUE;
            allQueriesCompleted();
        }
    }

    //Query handler
    void queryHandler(DataMsg* msg) {
        QueryMsg* queryMsg1 = (QueryMsg*)msg;
        QueryMsg queryResp;
        uint16_t i;

        queryResp.nodeId = queryMsg1->nodeId;
        queryResp.type = QUERY_RESP_MSG;
        queryResp.length = queryMsg1->length;

        DEBUG_PRINT("[queryHandler] from: %u\n", queryMsg1->nodeId);
        printArray("[queryHandler] queries:", queryMsg1->integers, queryMsg1->length);

        //Query completed message
        if (queryMsg1->length==0) {
            queryComplete[queryMsg1->nodeId%3] = TRUE;
            checkAllQueriesCompleted();
        }
        //Fetch numbers from numbers store
        for (i=0;i<queryMsg1->length;i++) {
            queryResp.integers[i] = integers[queryMsg1->integers[i]];
        }

        printArray("[queryHandler] responses:", queryResp.integers, queryResp.length);

        sendData((DataMsg*)(&queryResp));
    }

    //Query response handler
    void queryRespHandler(DataMsg* msg) {
        QueryMsg* queryResp1 = (QueryMsg*)msg;
        uint8_t i;

        //Not request / response from this node
        if (queryResp1->nodeId!=TOS_NODE_ID) {
            return;
        }

        //Update numbers
        for (i=0;i<queryResp1->length;i++) {
            uint16_t index1 = queryMsg.integers[i];
            uint16_t peerNumber = queryResp1->integers[i];

            if ((integers[index1]==0xffff)&&(peerNumber!=0xffff)) {
                DEBUG_PRINT("[queryRespHandler] Number from peer: [%u]=%u\n", index1, peerNumber);

                integers[index1] = peerNumber;
                unknownNumbers--;
            }
        }

        printArray("[queryRespHandler] responses:", queryResp1->integers, queryResp1->length);
        DEBUG_PRINT("[queryRespHandler] unknown: %u\n", unknownNumbers);

        //Update query state
        queryState++;

        //Query completed
        if (queryState>=2) {
            //Stop timeout timer
            call QueryTimeoutTimer.stop();

            //All numbers known
            if (unknownNumbers==0) {
                //All numbers of current node known
                DEBUG_PRINT("[queryRespHandler] All numbers of current node known.\n");

                //Do not handle number message
                handleNumberMsgFlag = FALSE;
                //Self query completed hook
                selfQueryCompleted();
                //Set and check query completion status of all nodes
                queryComplete[TOS_NODE_ID%3] = TRUE;
                checkAllQueriesCompleted();
            } else {
                //Continue check collect numbers
                DEBUG_PRINT("[queryRespHandler] Continue checkCollectNumbers.\n");
                //Reset query message length
                queryMsg.length = 0;

                post checkCollectNumbers();
            }
        }
    }

    //Query response timeout
    event void QueryTimeoutTimer.fired() {
        //No timeout
        if (queryState>=2) {
            return;
        }
        //Do not send packet again when all numbers known
        if (queryComplete[TOS_NODE_ID%3]) {
            return;
        }

        DEBUG_PRINT("[QueryTimeoutTimer.fired] Query timeout. Retransmitting...\n");

        //Reset state
        queryState = 0;
        //Start timeout timer
        call QueryTimeoutTimer.startOneShot(TIMEOUT);
        //Send data again
        sendData((DataMsg*)(&queryMsg));
    }

    //Partial result ACK handler
    void partialResultAckHandler(DataMsg* msg) {
        PartialResultMsg* resultMsg1 = (PartialResultMsg*)msg;

        //Update part result state
        if (resultMsg1->nodeId!=TOS_NODE_ID) {
            return;
        }

        DEBUG_PRINT("[partialResultAckHandler] Partial result sent. (COMPLETED)\n");

        //Update state
        partResultState++;

        //End of node A/C logic
        //The node should do nothing after execution of this function
    }

    //Partial result ACK timeout
    event void PartialResultTimeoutTimer.fired() {
        //No timeout
        if (partResultState>=1)
            return;

        DEBUG_PRINT("[PartialResultTimeoutTimer.fired] Query timeout. Retransmitting...\n");

        //Reset state
        partResultState = 0;
        //Start timeout timer
        call PartialResultTimeoutTimer.startOneShot(TIMEOUT);
        //Send data again
        sendData((DataMsg*)(&partResult));
    }

    //Send data task
    task void sendTask() {
        message_t* dataPacket;

        dataPacket = dataQueue+dataEnd%DATA_QUEUE_LEN;
        //Try to send packet
        if (call DataMsgSender.send(AM_BROADCAST_ADDR, dataPacket, sizeof(DataMsg))==SUCCESS) {
            //Busy state
            dataSendBusy = TRUE;

            call Leds.led1Toggle();
        } else {
            //Update queue end
            dataEnd++;
            //Next send task
            if (dataBegin>dataEnd) {
                post sendTask();
            }

            call Leds.led0Toggle();
        }
    }

    //Send data
    void sendData(DataMsg* msg) {
        message_t* packet;
        void* payload;

        //Queue full
        if (dataEnd+DATA_QUEUE_LEN<=dataBegin)
            return;

        //Get data and payload
        packet = dataQueue+dataBegin%DATA_QUEUE_LEN;
        payload = call DataMsgSender.getPayload(packet, sizeof(DataMsg));
        //Copy data
        memcpy(payload, msg, sizeof(DataMsg));

        //Start send task
        if (dataBegin==dataEnd) {
            post sendTask();
        }
        //Update queue begin
        dataBegin++;
    }

    //Data send done
    event void DataMsgSender.sendDone(message_t* packet, error_t err) {
        void* payload = call DataMsgSender.getPayload(packet, sizeof(DataMsg));
        DataMsg* msg = (DataMsg*)payload;

        //Non-busy state
        dataSendBusy = FALSE;
        //Queue end
        dataEnd++;
        //Next send task
        if (dataBegin>dataEnd) {
            post sendTask();
        }

        if (err==SUCCESS) {
            DEBUG_PRINT("[sendData] type: %u, nodeId: %u\n", msg->type, msg->nodeId);

            call Leds.led1Toggle();
        } else {
            call Leds.led0Toggle();
        }
    }
}
