#ifndef MSG_H
#define MSG_H

enum {
    //Message type
    AM_NUMBERMSG = 0,
    AM_RESULTMSG = 0,
    AM_RESULTACKMSG = 0,
    AM_DATAMSG = 4,
    //Data message type
    QUERY_MSG = 1,
    QUERY_RESP_MSG = 2,
    PARTIAL_RESULT_MSG = 3,
    PARTIAL_RESULT_ACK_MSG = 4,
    //Send interval
    SEND_INTERVAL = 10,
    //Integers
    N_INTEGERS = 2000,
    //Integers per task
    N_INTEGERS_PER_TASK = 10,
    //Max capacity
    MAX_DATA_LEN = 24,
    MAX_QUERIES = 10,
    MAX_RESULTS = 5,
    //Timeout
    TIMEOUT = 1500,
    //Data packet queue length
    DATA_QUEUE_LEN = 20,
    //Debug print interval
    DEBUG_PRINT_INTERVAL = 100,
    //Result node ID
    SOURCE_NODE = 1000,
    RESULT_NODE = 0
};

//Number message
typedef nx_struct NumberMsg {
    nx_uint16_t seqNumber;
    nx_uint32_t integer;
} NumberMsg;

//Result message
typedef nx_struct ResultMsg {
    nx_uint8_t groupId;
    nx_uint32_t max;
    nx_uint32_t min;
    nx_uint32_t sum;
    nx_uint32_t average;
    nx_uint32_t median;
} ResultMsg;

//Result ACK message
typedef nx_struct ResultAckMsg {
    nx_uint8_t groupId;
} ResultAckMsg;

//Data message
typedef nx_struct DataMsg {
    nx_uint8_t type;
    nx_uint8_t nodeId;
    nx_uint8_t data[MAX_DATA_LEN];
} DataMsg;

//Query and query response message
typedef struct QueryMsg {
    nx_uint8_t type;
    nx_uint8_t nodeId;
    uint8_t length;
    uint16_t integers[MAX_QUERIES];
} QueryMsg;

//Partial result and partial result ACK message
typedef struct PartialResultMsg {
    nx_uint8_t type;
    nx_uint8_t nodeId;
    uint32_t results[MAX_RESULTS];
} PartialResultMsg;

#endif
