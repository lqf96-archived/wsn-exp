configuration SenseAppC {
}

implementation {
    components SenseC, MainC, LedsC;

    components new TimerMilliC() as TimerSend;

    components ActiveMessageC;
    components new AMSenderC(AM_NUMBERMSG);

    SenseC -> MainC.Boot;
    SenseC.Leds -> LedsC;

    SenseC.TimerSend -> TimerSend;

    SenseC.Packet -> AMSenderC;
    SenseC.AMPacket -> AMSenderC;
    SenseC.RadioControl -> ActiveMessageC;
    SenseC.AMSend -> AMSenderC;
}
