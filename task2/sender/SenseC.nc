//SenseC.nc: Sense node component
#include "Timer.h"
#include "printf.h"

#include "Msg.h"

module SenseC {
    uses {
        interface Boot;

        interface Leds;

        interface Timer<TMilli> as TimerSend;

        interface Packet;
        interface AMPacket;
        interface AMSend;
        interface SplitControl as RadioControl;
    }
}

implementation {
    //Integers
    uint16_t integers[N_INTEGERS];
    //Sequence number
    uint16_t seqNumber = 0;

    message_t packet;
    NumberMsg msg;
    bool sendBusy = FALSE;

    event void Boot.booted() {
        uint32_t i;

        for (i=0;i<N_INTEGERS;i++) {
            integers[i] = i;
        }

        call RadioControl.start();
    }

    event void RadioControl.startDone(error_t err) {
        if (err == SUCCESS) {
            //Start timer
            call TimerSend.startPeriodic(SEND_INTERVAL);
        } else {
            call RadioControl.start();
        }
    }

    event void RadioControl.stopDone(error_t error) {
        printf("Radio control stop.\n");
    }

    event void TimerSend.fired() {
        //Still busy; do not send
        if (sendBusy)
            return;

        //Copy message data
        msg.seqNumber = seqNumber%N_INTEGERS;
        msg.integer = integers[seqNumber%N_INTEGERS];
        seqNumber++;

        //Copy memory
        memcpy(call AMSend.getPayload(&packet, sizeof(NumberMsg)), &msg, sizeof(NumberMsg));
        //Send packet
        if (call AMSend.send(AM_BROADCAST_ADDR, &packet, sizeof(NumberMsg))==SUCCESS) {
            sendBusy = TRUE;
            call Leds.led1Toggle();
        } else {
            call Leds.led0On();
        }
    }

    event void AMSend.sendDone(message_t* sent_packet, error_t err) {
        sendBusy = FALSE;

        if (err==SUCCESS) {
            call Leds.led1Toggle();
        } else {
            call Leds.led0On();
        }
    }
}
