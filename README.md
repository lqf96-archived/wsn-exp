# WSN Experiment
Wireless Sensor Network Experiment

## File Structure
* `task1`: Multi-hop data collection experiment.
  - `base-station`: Base station code.
  - `forwarder`: Forwarder node code.
  - `senser`: Sense node code.
  - `visualization`: Visualization tool code, modified from Oscilloscope code in TinyOS programming examples.
* `task2`: Distributed computation experiment.
  - `receiver`: Code for nodes that receive numbers and do calculations.
  - `sender`: Number sender node.

## License
[GNU GPLv3](LICENSE)
